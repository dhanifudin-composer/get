# Get

A simple trait to get property value from some class or trait

# Install

First, add repositories into your composer.json then run ```composer update```.
More info you can look [here](http://dhanifudin.bitbucket.org/packagist).

```json
{
  "repositories": [
    {
      "type": "composer",
      "url": "http://dhanifudin.bitbucket.org/packagist"
    }
  ],
  "require": {
    "dhanifudin/get": "1.0"
  }
}
```

# Features
* Get integer value ```intValue($property, $default = 0)```
* Get string value ```stringValue($property, $default = '')```
* Get array value ```arrayValue($property, $default = array())```

# Usage

```php
<?php

namespace A\Random\Namespaces;

use Dhanifudin\Get;

class SomeClass {

	use Get;

	public function getSchema() {
		return $this->arrayValue('schema', array());
	}

}
```
