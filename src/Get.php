<?php

namespace Dhanifudin;

trait Get {

	protected function intValue($property, $default = 0) {
		return (isset($this->{$property}) && is_numeric($this->{$property}))
			? $this->{$property}
			: $default;
	}

	protected function stringValue($property, $default = '') {
		return (isset($this->{$property}))
			? $this->{$property}
			: $default;
	}

	protected function arrayValue($property, $default = array()) {
		return (isset($this->{$property}) && is_array($this->{$property}))
			? $this->{$property}
			: $default;
	}

}
